import React, { Component } from 'react';
import './App.css';
import logo from './React-logo.png';
import pic1 from './pic-1.jpg';
import pic2 from './pic-2.jpg';
import pic3 from './pic-3.jpg';
import pic4 from './pic-4.jpg';

const Li = (props) => {
    return (
        <li><a href="#">{props.val}</a></li>
    )
};

const Navbar = () => {
    return (
        <nav className="uk-navbar-container" uk-navbar>
            <div className="uk-navbar-left">
                <ul className="uk-navbar-nav">
                    <Li val="Home"/>
                    <Li val="About us"/>
                    <Li val="Contacts"/>
                </ul>
            </div>
        </nav>
    )
};

const Header = (props) => {
    return (
        <header>
            <div className="uk-container uk-flex uk-flex-between uk-flex-middle">
                <a href="#" className="logo">
                    <img src={props.src} alt=""/>
                </a>
                <Navbar/>
            </div>
        </header>
    )
};

const MainBlockItem = (props) => {
    return (
        <div>
            <div className="uk-card-media-top">
                <img src={props.img} alt="" />
            </div>
            <div className="uk-card uk-card-primary uk-card-body">
                <h3 className="uk-card-title">{props.title}</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
        </div>
    )
};

const MainBlock = (props) => {
    return (
        <div className="main-block">
            <div className="uk-container">
                <h1>{props.title}</h1>
                <div className="uk-grid uk-child-width-1-4">
                    <MainBlockItem title="Card-1" img={pic1}/>
                    <MainBlockItem title="Card-2" img={pic2}/>
                    <MainBlockItem title="Card-3" img={pic3}/>
                    <MainBlockItem title="Card-4" img={pic4}/>
                </div>
            </div>
        </div>
    )
};

const Footer = (props) => {
    return (
        <footer>
            <div className="uk-container uk-flex uk-flex-between uk-flex-middle">
                <span>Tel: {props.tel}</span>
                <Navbar/>
            </div>
        </footer>
    )
};

const page = (
    <React.Fragment>
        <Header src={logo}/>
        <MainBlock/>
        <Footer tel="+996559773283"/>
    </React.Fragment>
);

class App extends Component {
  render() {
    return (page);
  }
}

export default App;
