import React, { Component } from 'react';
import './App.css';
import star from './star.jpg';
import harry from './harry.jpg';
import lord from './lord.jpg';

const Cinema = (props) => {
    return (
        <div>
            <div className="uk-card uk-card-default">
                <div className="uk-card-media-top">
                    <img src={props.src} alt=""/>
                </div>
                <div className="card-text">
                    <h3 className="uk-card-title"><i>Название фильма:</i> {props.title}</h3>
                    <p>Год выпуска: <b>{props.year}</b></p>
                </div>
            </div>
        </div>
    );
};

const films = (
    <div className="uk-grid uk-child-width-1-6">
        <Cinema src={lord} title="Властелин колец: Две крепости" year="2002" />
        <Cinema src={harry} title="Гарри Поттер" year="1993" />
        <Cinema src={star} title="Звездные войны" year="1993" />
    </div>
);

class App extends Component {
  render() {
      return (films)
  }
}

export default App;
